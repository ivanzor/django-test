FROM python:3.8-slim
RUN mkdir /app
WORKDIR /app
COPY ./ /app
RUN pip3 install -r requirements/main.txt
RUN pip3 install -r requirements/dev.txt

